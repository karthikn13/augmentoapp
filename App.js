/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import RootStack from './app/routes';
import AppStyles from './app/config/styles';


const App = () => {
  return (
    <View style={styles.container}>
    <StatusBar barStyle="light-content" backgroundColor={AppStyles.color.COLOR_GREY}/>
     <RootStack />
     </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
     },
});

export default App;
