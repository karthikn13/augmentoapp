#steps to run the application

1. After cloning the repository git read and write permission to the cloned folder by the following command,

    sudo chmod -R 777 "cloned folder name"

2. Inside the project directory run the following command,

   npm install --save

3. Once node modules gets downloaded run the following command,

   react-native start

   react-native run-android

 

