import React, { Component } from 'react'
import { ActivityIndicator, View, StyleSheet, Image, Text,TouchableOpacity } from 'react-native';
import AppStyles from '../config/styles';


const CardView = (props) => {
    return (
        <View style={styles.cardContainer}>
            <TouchableOpacity onPress={props.onPress} >
            <View style={styles.imageContainer}>

                <Image source={{ uri: props.imageSource }} style={styles.imageStyle} />
            </View>
            <View style={styles.titleContainer}>
                <Text>{props.name}</Text>
            </View>
            <View style={styles.priceContainer}>
                <Text style={styles.price}>{props.price}</Text>
            </View>
            </TouchableOpacity>
        </View>
    )

}
const styles = StyleSheet.create({

    cardContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        margin:5,
        width:125,
        backgroundColor: AppStyles.color.COLOR_WHITE

    },
    imageContainer: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: AppStyles.color.COLOR_WHITE
    },
    titleContainer: {

        
        height:40,
        padding:5,
        backgroundColor: AppStyles.color.COLOR_WHITE
    },
    priceContainer: {
        padding:5,
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: AppStyles.color.COLOR_WHITE
    },
    imageStyle:
    {
        width: 125,
        height: 150

    },
    price:{
       fontFamily:'Regular',
       fontSize:14,
       color:'red',
       alignSelf:'flex-end'
       
    }
});



export default CardView;