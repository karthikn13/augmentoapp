import React from "react";
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Home from '../app/screens/Home';
import Gift from '../app/screens/Gift';
import Cart from '../app/screens/Cart';
import Account from '../app/screens/Account';
import Details from '../app/screens/Details';
import AppStyles from '../app/config/styles'

const AppNavigator = createStackNavigator({
    Home: {
        screen: Home,
    },
    Details: {
        screen: Details,
    },
});
const BottomTab = createMaterialBottomTabNavigator({
    Home: { screen: AppNavigator },
    Gift: { screen: Gift },
    Cart: { screen: Cart },
    Account: { screen: Account },
},
    {  
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
              const { routeName } = navigation.state;
              let IconComponent = MaterialCommunityIcon;
              let iconName;
              if (routeName === 'Home') {
                iconName = 'home';
              } else if (routeName === 'Gift') {
                iconName = 'gift-outline';
              }
              else if (routeName === 'Cart') {
                iconName = 'cart';
              }else if (routeName === 'Account') {
                iconName = 'account-circle';
              }
              
              
              return <IconComponent color={tintColor} name={iconName} size={25}  />;
            },
          }), 
        initialRouteName: "Home",
        activeColor: 'red',
        inactiveColor: 'grey',
        barStyle: { backgroundColor: AppStyles.color.COLOR_WHITE },
    });


const RootStack = createAppContainer(BottomTab);

export default RootStack;
