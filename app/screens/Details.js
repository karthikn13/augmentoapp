import * as React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import AppStyles from '../config/styles';


export default class Details extends React.Component {

    static navigationOptions = {
        title:'Details'
    }
    render() {
        let item = this.props.navigation.state.params.item;

        return (
            <View style={styles.mainContainer}>
                <View style={styles.imageContainer}>
                    <Image source={{ uri: item.image }} style={{ width: 200, height: 300 }} />
                </View>
                <View style={styles.nameContainer}>
                    <Text style={styles.title}>{item.title}</Text>
                    <Text style={styles.price}>{item.price}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({


    mainContainer: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    imageContainer: { margin: 10, alignItems: 'center', justifyContent: 'center' },
    nameContainer: {
        padding: 10,
       
        flexDirection: 'row',
        justifyContent: 'space-around'
        
    },
    title: {
        fontSize: AppStyles.fontSize.INPUT_INFO,
        fontWeight: 'bold',



    },
    price: {
        color: AppStyles.color.COLOR_RED,
        fontSize: AppStyles.fontSize.INPUT_INFO,
        fontWeight: 'bold',

    }

});


