import * as React from 'react';
import { View, Dimensions, ScrollView, Button, Text, StyleSheet, Alert, Animated, Image } from 'react-native';
import ActivityLoading from '../common/ActivityLoading';
import { getAllData } from '../../app/services/Apiservice';
import CardView from '../common/CardView';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { FlatList } from 'react-native-gesture-handler'

import AppStyles from '../config/styles';

const width = Dimensions.get('window').width;
const FIXED_BAR_WIDTH = width
const BAR_SPACE = 0
const corosalImages= [
    'https://source.unsplash.com/1024x768/?nature',
    'https://source.unsplash.com/1024x768/?water',
    'https://source.unsplash.com/1024x768/?girl',
    'https://source.unsplash.com/1024x768/?tree'
]

export default class Home extends React.Component {

    numItems = corosalImages.length
    itemWidth = (FIXED_BAR_WIDTH / this.numItems) - ((this.numItems - 1) * BAR_SPACE)
    animVal = new Animated.Value(0)

    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.arrayHolder = [];
        this.state = {
           
            movieData: [],
            activityLoader: true
        };

    }

    componentDidMount() {
        this.loadMovieData();
    }

    loadMovieData = () => {
        getAllData()
            .then((responseJson) => {
                if (responseJson) {
                    this.setState({
                        activityLoader: false,
                        movieData: responseJson
                    })
                    this.arrayHolder = responseJson


                }
            }).catch((error) => {
                Alert.alert('No Internet connection.\n Please check your internet connection \nor try again');
            });

    }

    renderGridItem(item) {
        return (
            <View>
                <CardView imageSource={item.image} name={item.title} price={item.price} onPress={() => this.props.navigation.navigate('Details', { item })} />
            </View>
        )

    }

    render() {
        let imageArray = []
        let barArray = []
        corosalImages.forEach((image, i) => {

            const thisImage = (
                <Image
                    key={`image${i}`}
                    source={{ uri: image }}
                    style={{ width: width }}
                />
            )
            imageArray.push(thisImage)
            console.log('image Array', imageArray.length)
            const scrollBarVal = this.animVal.interpolate({
                inputRange: [width * (i - 1), width * (i + 1)],
                outputRange: [-this.itemWidth, this.itemWidth],
                extrapolate: 'clamp',
            })

            const thisBar = (

                <View
                    key={`bar${i}`}
                    style={[
                        styles.track,{
                            width: this.itemWidth,
                            marginLeft: i === 0 ? 0 : BAR_SPACE,
                        },
                    ]}>
                    <Animated.View

                        style={[
                            styles.bar,
                            {
                                width: this.itemWidth,
                                transform: [
                                    { translateX: scrollBarVal },
                                ],
                            },
                        ]}
                    />
                </View>
            )
            barArray.push(thisBar)
            console.log('Array', barArray.length)
        });

        if (this.state.activityLoader) {
            return (
                <ActivityLoading size="large" />
            );
        }
        return (

            <View style={styles.mainContainer} >
                <View style={{ height: 250,marginBottom:20}}>
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={5}
                        pagingEnabled
                        onScroll={
                            Animated.event(
                                [{ nativeEvent: { contentOffset: { x: this.animVal } } }]
                            )
                        }>
                        {imageArray}

                    </ScrollView>
                    <View style={styles.barContainer}>{barArray}</View>
                </View>

                <View style={styles.movieListContainer}>
                    <FlatList
                        data={this.state.movieData}
                        renderItem={({ item }) => this.renderGridItem(item)
                        }
                        keyExtractor={item => item.id}
                        numColumns={3}
                    />
                </View>

            </View>

        );

    }
}

const styles = StyleSheet.create({


    mainContainer: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    movieListContainer: { flex: 1, justifyContent: 'flex-start', backgroundColor: AppStyles.color.COLOR_GREY, paddingLeft: 3 }
    ,barContainer: {
        position: 'relative',
        flexDirection: 'row',
      },
      track: {
        backgroundColor: AppStyles.color.COLOR_WHITE,
        overflow: 'hidden',
        height: 5,
      },
      bar: {
        backgroundColor: AppStyles.color.COLOR_RED,
        height: 5,
        
      },
});

