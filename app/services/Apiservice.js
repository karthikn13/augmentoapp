const URL = 'https://api.androidhive.info/json/movies_2017.json';

export const getAllData = () => {
  let data = {
    method: 'GET',
    credentials: 'same-origin',
    mode: 'same-origin',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      
    }
  }
  return fetch(URL, data)
    .then((response) => response.json())
}





